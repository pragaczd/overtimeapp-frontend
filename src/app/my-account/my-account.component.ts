import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {TokenStorageService} from '../service/token-storage.service';
import {User} from '../data/user';
import {UserService} from '../service/user.service';
import {environment} from '../../environments/environment';
import {HttpClient, HttpEventType, HttpHeaders, HttpResponse} from '@angular/common/http';
import {ImageService} from '../service/image.service';

@Component({
  selector: 'app-my-account',
  templateUrl: './my-account.component.html',
  styleUrls: ['./my-account.component.scss']
})
export class MyAccountComponent implements OnInit {

  private roles: string[];
  isLoggedIn = false;
  username: string;
  userData: User;
  file: File;
  filePath: string;
  selectedFiles: FileList;
  currentFile: File;
  progress = 0;
  message = '';
  editable = false;
  description: string;

  constructor(private tokenStorageService: TokenStorageService, private userService: UserService, private imageService: ImageService) {
  }

  ngOnInit(): void {
    this.imageService.getFilePath().subscribe(
      (response: any) => {
        this.filePath = '..\\..\\' + response;
      }
    );

    this.isLoggedIn = !!this.tokenStorageService.getToken();

    if (this.isLoggedIn) {
      const user = this.tokenStorageService.getUser();
      this.roles = user.roles;

      this.username = user.user;
      this.userService.getUserByUsername(this.username).subscribe(
        (response: any) => {
          this.userData = response;
        });
    }
  }

  selectFile(event) {
    this.selectedFiles = event.target.files;
  }

  upload() {
    this.progress = 0;

    this.currentFile = this.selectedFiles.item(0);
    this.imageService.upload(this.currentFile).subscribe(
      event => {
        if (event.type === HttpEventType.UploadProgress) {
          this.progress = Math.round(100 * event.loaded / event.total);
        } else if (event instanceof HttpResponse) {
          this.message = event.body.message;
        }
      },
      err => {
        this.progress = 0;
        this.message = 'Could not upload the file!';
        this.currentFile = undefined;
      });

    this.selectedFiles = undefined;
  }

  deleteFile() {
    this.imageService.delete().subscribe(
      response => {
        window.location.reload();
      }
    );
  }

  setEditable() {
    this.editable = !this.editable;
  }

  saveDescription() {
    this.userService.saveEditedUser(this.userData, this.description).subscribe(
      response => {
        this.editable = !this.editable;
        console.log(this.userData);
      }
    );
  }

}
