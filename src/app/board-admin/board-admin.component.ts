import {Component, OnInit} from '@angular/core';
import {UserService} from '../service/user.service';
import {User} from '../data/user';

@Component({
  selector: 'app-board-admin',
  templateUrl: './board-admin.component.html',
  styleUrls: ['./board-admin.component.scss']
})
export class BoardAdminComponent implements OnInit {

  users: User[];

  constructor(private userService: UserService) {
  }

  ngOnInit(): void {
    this.search();
  }

  search() {
    this.userService.getAllUsers()
      .subscribe((users: any[]) => {
        this.users = users;
      });
  }

  deleteSearch() {
    this.users = [];
  }
}
