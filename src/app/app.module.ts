import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';

import {AppComponent} from './app.component';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';
import {HomeComponent} from './home/home.component';
import {BoardAdminComponent} from './board-admin/board-admin.component';
import {BoardUserComponent} from './board-user/board-user.component';
import {HeaderComponent} from './header/header.component';
import {JwtHelperService, JwtModule} from '@auth0/angular-jwt';
import {MyAccountComponent} from './my-account/my-account.component';
import {AuthInterceptor} from './interceptor/auth.interceptor';
import {FooterComponent} from './footer/footer.component';
import {OvertimeRecordsComponent} from './overtime-records/overtime-records.component';
import {MyOvertimeComponent} from './my-overtime/my-overtime.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    BoardAdminComponent,
    BoardUserComponent,
    HeaderComponent,
    MyAccountComponent,
    FooterComponent,
    OvertimeRecordsComponent,
    MyOvertimeComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    JwtModule,
    BrowserAnimationsModule,
    ReactiveFormsModule
  ],
  providers: [JwtHelperService, {
    provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule {
}
