import {Component, OnInit, Optional} from '@angular/core';
import {Overtime} from '../data/overtime';
import {OvertimeService} from '../service/overtime.service';
import {TokenStorageService} from '../service/token-storage.service';
import {User} from '../data/user';
import {UserService} from '../service/user.service';

@Component({
  selector: 'app-overtime-records',
  templateUrl: './overtime-records.component.html',
  styleUrls: ['./overtime-records.component.scss']
})
export class OvertimeRecordsComponent implements OnInit {

  overtimes: Overtime[];
  form: any = {};
  username: string = '';

  constructor(private overtimeService: OvertimeService, private tokenStorageService: TokenStorageService, private userService: UserService) {
  }

  ngOnInit(): void {

    this.overtimeService.getAllRecords().subscribe(
      (overtimes: any[]) => {
        this.overtimes = overtimes;

      }
    );
  }

  searchUser() {
    this.overtimeService.getAllOvertimeRecordsForUserByUsername(this.username).subscribe(
      (overtime: any[]) => {
        this.overtimes = overtime;
      }
    );
  }
}
