import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OvertimeRecordsComponent } from './overtime-records.component';

describe('OvertimeRecordsComponent', () => {
  let component: OvertimeRecordsComponent;
  let fixture: ComponentFixture<OvertimeRecordsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OvertimeRecordsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OvertimeRecordsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
