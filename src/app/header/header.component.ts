import {Component, OnInit} from '@angular/core';
import {TokenStorageService} from '../service/token-storage.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  private roles: string[];
  isLoggedIn = false;
  showAdminComponents = false;
  showUserComponents = false;
  username: string;

  constructor(private tokenStorageService: TokenStorageService) {
  }

  ngOnInit() {
    this.isLoggedIn = !!this.tokenStorageService.getToken();

    if (this.tokenStorageService.isAuthenticated()) {
      if (this.isLoggedIn) {
        const user = this.tokenStorageService.getUser();
        this.roles = user.roles;

        this.showAdminComponents = this.roles.includes('ROLE_ADMIN');
        this.showUserComponents = this.roles.includes('ROLE_USER');

        this.username = user.user;
      }
    } else {
      this.isLoggedIn = false;
    }
  }

  logout() {
    this.tokenStorageService.signOut();
    window.location.reload();
  }

  signUp() {
    this.reloadPage();
  }

  reloadPage() {
    window.location.reload();
  }
}
