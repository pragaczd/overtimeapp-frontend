import DateTimeFormat = Intl.DateTimeFormat;

export class Overtime {
  constructor(public id: number,
              public idUsername: number,
              public username: string,
              public dateAdd: Date,
              public balance: number,
              public recentlyAddedOvertime: number,
              public overtimeDate: Date,
              public fullOvertimeDate: Date
              ) {
  }
}

