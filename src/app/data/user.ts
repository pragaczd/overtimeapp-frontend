export class User {
  constructor(public id: number,
              public username: string,
              public email: string,
              public firstName: string,
              public lastName: string,
              public password: string,
              public activate: boolean,
              public roles: string,
              public position: string,
              public description: string,
              public authdata?: string)
  {}
}
