import {Injectable} from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import {Observable} from 'rxjs';
import {TokenStorageService} from '../service/token-storage.service';
import {environment} from '../../environments/environment';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private tokenStorageService: TokenStorageService) {
  }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {

    // let contentType = 'application/json';
    //
    // if (request.body instanceof FormData) {
    //   contentType = 'multipart/form-data';
    // }

    const isLoggedIn = this.tokenStorageService.isAuthenticated();
    const isApiUrl = request.url.startsWith(environment.apis.api);
    if (isLoggedIn && isApiUrl) {
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${JSON.parse(new TokenStorageService().getToken()).jwt}`,
          'Content-Type': 'application/json'
        }
      });
      if (request.body instanceof FormData) {
        request = request.clone({headers: request.headers.delete('Content-Type', 'application/json')});
      }
    }

    return next.handle(request);
  }
}
