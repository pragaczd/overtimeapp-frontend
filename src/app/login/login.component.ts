import {Component, OnInit} from '@angular/core';
import {AuthService} from '../service/auth.service';
import {TokenStorageService} from '../service/token-storage.service';
import {JwtHelperService, JwtInterceptor, JwtModule} from '@auth0/angular-jwt';
import {ImageService} from '../service/image.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  form: any = {};
  isLoggedIn = false;
  isLoginFailed = false;
  errorMessage = '';
  roles: string = '';
  user: string = '';
  filePath: string;

  constructor(private authService: AuthService, private tokenStorage: TokenStorageService, private imageService: ImageService) {
  }

  ngOnInit() {
    if (this.tokenStorage.isAuthenticated()) {
      if (this.tokenStorage.getToken()) {
        this.isLoggedIn = true;
        this.roles = this.tokenStorage.getUser().roles;
        this.user = this.tokenStorage.getUser().user;

        this.imageService.getFilePath().subscribe(
          (response: any) => {
            this.filePath = '..\\..\\' + response;
          }
        );
      }
    } else {
      this.isLoggedIn = false;
      this.filePath = '..\\..\\';
    }
  }

  onSubmit() {
    this.authService.login(this.form).subscribe(
      data => {
        const decodedToken = this.decodedToken(data);
        this.tokenStorage.saveToken(JSON.stringify(data));
        this.tokenStorage.saveUser(JSON.stringify({user: decodedToken.sub, roles: decodedToken.roles, userId: decodedToken.userId}));

        this.isLoginFailed = false;
        this.isLoggedIn = true;
        this.roles = this.tokenStorage.getUser().roles;
        this.reloadPage();
      },
      err => {
        this.errorMessage = err.error.message;
        this.isLoginFailed = true;
      }
    );
  }

  reloadPage() {
    window.location.reload();
  }

  private decodedToken(data: object) {
    const helperService = new JwtHelperService();
    return helperService.decodeToken(JSON.stringify(data));
  }
}
