import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {environment} from '../../environments/environment';
import {User} from '../data/user';
import {TokenStorageService} from './token-storage.service';
import set = Reflect.set;
import {catchError, map} from 'rxjs/operators';
import {AuthGuardService} from './auth-guard.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) {
  }

  getUserByUsername(username: string) {
    return this.http.get<User>(environment.apis.api + `/users/getUserByUsername/${username}`);
  }

  getAllUsers() {
    return this.http.get<User[]>(environment.apis.api + '/users');
  }

  saveEditedUser(user, description) {
    return this.http.post(environment.apis.api + `/users/saveEditedUser`, {
      username: user.username,
      description: description
    })
      ;
  }
}
