import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Overtime} from '../data/overtime';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class OvertimeService {

  constructor(private http: HttpClient) {
  }

  getAllOvertimeRecordsForUser(userId) {
    return this.http.get<Overtime[]>(environment.apis.api + `/overtime/allRecords/${userId}`);
  }

  addOvertime(overtime, userId, username) {
    return this.http.post(environment.apis.api + `/overtime/addOvertimeForUser`, {
      recentlyAddedOvertime: overtime.recentlyAddedOvertime,
      overtimeDate: overtime.overtimeDate,
      idUsername: userId,
      username: username
    });
  }

  getAllRecords() {
    return this.http.get(environment.apis.api + '/overtime/allRecords');
  }

  getAllOvertimeRecordsForUserByUsername(username) {
    return this.http.get<Overtime[]>(environment.apis.api + `/overtime/allRecordsByUsername/${username}`);
  }

}
