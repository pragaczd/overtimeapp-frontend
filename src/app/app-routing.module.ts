import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LoginComponent} from './login/login.component';
import {HomeComponent} from './home/home.component';
import {BoardAdminComponent} from './board-admin/board-admin.component';
import {BoardUserComponent} from './board-user/board-user.component';
import {RegisterComponent} from './register/register.component';
import {AuthGuardService} from './service/auth-guard.service';
import {MyAccountComponent} from './my-account/my-account.component';
import {OvertimeRecordsComponent} from './overtime-records/overtime-records.component';
import {MyOvertimeComponent} from './my-overtime/my-overtime.component';

const routes: Routes = [
  {path: 'home', component: HomeComponent},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'myAccount', component: MyAccountComponent, canActivate: [AuthGuardService]},
  {path: 'user', component: BoardUserComponent, canActivate: [AuthGuardService]},
  {path: 'admin', component: BoardAdminComponent, canActivate: [AuthGuardService]},
  {path: 'overtimeRecords', component: OvertimeRecordsComponent, canActivate: [AuthGuardService]},
  {path: 'myOvertime', component: MyOvertimeComponent, canActivate: [AuthGuardService]},
  {path: '', redirectTo: 'home', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
