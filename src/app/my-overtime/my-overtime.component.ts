import {Component, OnInit} from '@angular/core';
import {Overtime} from '../data/overtime';
import {OvertimeService} from '../service/overtime.service';
import {TokenStorageService} from '../service/token-storage.service';
import {UserService} from '../service/user.service';
import {User} from '../data/user';

@Component({
  selector: 'app-my-overtime',
  templateUrl: './my-overtime.component.html',
  styleUrls: ['./my-overtime.component.scss']
})
export class MyOvertimeComponent implements OnInit {

  form: any = {};
  overtimes: Overtime[];
  user: User;

  constructor(private tokenStorageService: TokenStorageService, private overtimeService: OvertimeService) {
  }

  ngOnInit(): void {
    this.overtimeService.getAllOvertimeRecordsForUser(this.tokenStorageService.getUser().userId)
      .subscribe((response: any) => {
          this.overtimes = response;
          console.log(response);
        }
      );
  }

  addOvertime() {
    this.overtimeService.addOvertime(this.form, this.tokenStorageService.getUser().userId, this.tokenStorageService.getUser().user).subscribe(
      data => {
        this.reloadPage();
      }
    );
  }

  private reloadPage() {
    window.location.reload();
  }
}
